# 3D_4G

3D Vier Gewinnt in https://p5js.org das ich in meiner Freizeit zum Üben entwickle um darauf aufbauend mit handgeschriebenen oder auf maschinellem Lernen basierenden Bots zu Experimentieren.

# Setup

Das ganze muss z.B. lokal in lampp gehostet werden. (ich habe es in einem eigenen Ordner unter htdocs abgelegt.)

Die 3d4g Dateien müssen in einem Ordner neben dem p5 Ordner in dem der p5.js Source und gegebenenfalls zukünftig p5 addons enthalten sind liegen. Dann kann das 3D Vier Gewinnt einfach lokal im Browser über die index.html aufgerufen werden.

- htdocs
    - [p5_projekte]
        - [3d4g]
            - index.html
            - 3d4g.js
            - gameboard.js
            - constants.js
        - [p5]
            - [addons]
                - ...
            - p5.js
            - p5.min.js
            - ...
    - ...