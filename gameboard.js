
let beads = [];
function newGameboard(){
	for(let x = 0;x<4; x+=1){
		beads.push([]);
		for(let y = 0; y<4; y+=1){
			beads[x].push([]);
			for(let z = 0;z<4; z+=1){
				beads[x][y].push(0);
			}
		}
		//print(x);console.table(beads[x]);
	}
	return beads;
}

//Returns the z coordinate and color of the Highest bead in the stack. [0, 0] if empty.
function getHighest(x, y){
	let i = 0, b = 0;
	while(i<=3 && beads[x][y][i] != 0){
		b = beads[x][y][i];
		i++;
	}
		return [i, b];
}

function dropBead(coordinate = [0, 0], player=1){
	let finalPosition = getHighest(coordinate[0], coordinate[1])[0];
	if(finalPosition[0] != 3){
		beads[coordinate[0]][coordinate[1]][finalPosition] = player;
	}else{
		return -2;
	}
	return finalPosition+1;
}


function checkBead(bead = [0, 0, 0]){
	let tmp_intersecting_lines = getIntersectingLines(bead);
	let result = 0;
  	for(let y = 0; y < tmp_intersecting_lines.length; y++){
		let tmp_r = checkAxis(tmp_intersecting_lines[y]);
		if(tmp_r != 0){
			result = tmp_r;
		}
	}
	return result;
}

function checkAxis(axis = []){
	let line = [];
	let result = 0;
	if(axis.length == 4){
		for(let i = 0; i <= 3; i ++){
			line.push(beads[axis[i][0]][axis[i][1]][axis[i][2]]);
		}
		let tmp_result = checkLine(line);
		if(tmp_result != 0){
			result = tmp_result;
		}
	}else{
		print("ERROR: passed wrong size of line array.");
		return -2;
	}
	return result;
}

function beadsOnAxis(axis = []){
	let line = [];
	let result = {"-1": 0, "0": 0, "1": 0};
	if(axis.length == 4){
		for(let i = 0; i <= 3; i ++){
			result[beads[axis[i][0]][axis[i][1]][axis[i][2]]]++;
		}
	}else{
		print("ERROR: passed wrong size of line array.");
		return -2;
	}
	return result;
}



// functions not bound to gameboard but related.

// returns a list of all intersecting lines of this coordinate
function getIntersectingLines(bead = [0, 0, 0]){
	let lines = [];
	
	// straights calculate
	for(let i = 0; i < 3; i++){
		// temporary variables
		let cur = Array.from(bead);
		let tmp_line = [];
		// calculate line
		for(let j = 0; j < 4; j++){
			cur[i] = j;
			tmp_line.push(Array.from(cur));
		}
		// add line to output
		lines.push(Array.from(tmp_line));
	}
	
	// diagonals check if on any
	let dir = [[1, 2], [0, 2], [0, 1]];
	for(let i = 0; i < dir.length; i++){
		// temporary variables
		let cur = Array.from(bead);
		let tmp_line = [];
		let is_in = false;
		
		// generate line
		for( let j = 0; j <= 3; j++ ){
			cur[dir[i][0]] = j;
			cur[dir[i][1]] = j;
			if(cur[0] == bead[0] && cur[1] == bead[1] && cur[2] == bead[2]){is_in = true;}
			tmp_line.push(Array.from(cur));
		}
		
		// if intersecting add to output
		if(is_in){
			lines.push(Array.from(tmp_line));
		}
		
		// reset temporary variables
		cur = Array.from(bead);
		tmp_line = [];
		is_in = false;
		// generate line
		for( let j = 0; j <= 3; j++ ){
			cur[dir[i][0]] = j;
			cur[dir[i][1]] = 3-j;
			if(cur[0] == bead[0] && cur[1] == bead[1] && cur[2] == bead[2]){
				is_in = true;
			}
			tmp_line.push(Array.from(cur));
		}
		
		// if intersecting add to output
		if(is_in){
			lines.push(Array.from(tmp_line));
		}
	}
	
	// quers check if on any then
	// +++ [000]>[333]
	// ++- [003]>[330]
	// +-+ [030]>[303]
	// +-- [033]>[300]
	let n = 0;
	for(let i = 0; i <= 3; i++){
		// temporary variables
		let cur = [0, 0, 0];
		let is_in = false;
		let tmp_line = [];
		// set y & z to top most bead if direction is -1 on these axis
		if(i&2==2){
			cur[1] = 3;
		}
		if(i&1==1){
			cur[2] = 3;
		}
		// generate line
		for(let j = 0; j <= 3; j++){
			cur[0] = j;
			cur[1] = (j*((i&2)!=2))+((3-j)*((i&2)==2));
			cur[2] = (j*((i&1)!=1))+((3-j)*((i&1)==1));
			if(cur[0] == bead[0] && cur[1] == bead[1] && cur[2] == bead[2]){
				is_in = true;
			}
			tmp_line.push(Array.from(cur));
		}
		// if intersecting add to output
		if(is_in){
			lines.push(Array.from(tmp_line));
		}
	}
	return lines;
}

function checkLine(line = [0, 0, 0, 0]){
	let result = 0;
	if( line[0] == line[1] && 
	    line[0] == line[2] && 
	    line[2] == line[3] 
	   ){
		result = line[0];
	}
	return result;
}
