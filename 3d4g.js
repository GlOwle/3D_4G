/*
Coordinates of the Beads:
in the programm the 'code'/'c' coordinates are used, 
the interface will use the 'play'/'p' coordinates for 
droping beads, as they are less likely to be accidentially be swapped (z is not realy relevant as players can only drop on top of one stake.

beads[x][y][z]

      |     |     |     |           
y/row |     |     |     |           
c p   | |   | |   | |   | |         
 \ \  | |   | |   | |   | |      z/layer
  3 D \-|-|-\-|-|-\-|-|-\ | |    c  p
   \ \ \| |  \| |  \| |  \| |    |  |
    2 C \-|-|-\-|-|-\-|-|-\ | |  3  fourth
     \ \ \| |  \| |  \| |  \| |  2  third
      1 B \-|---\-|---\-|---\ |  1  second
       \ \ \|    \|    \|    \|  0  first
        0 A \-----\-----\-----\     
            0  —  1  —  2  —  3 — code x/collumn
            1  —  2  —  3  —  4 — play

————————————————————————————————————————————————————————————————————————————
Player Values
The beads of player one are represented by an 1 and the ones of player 2 are represented by a -1. if a spot contains no bead it is represented by a 0.

v1 vs r 10.4m 500/0/0
v2 vs r 17.0m 500/0/0

r vs v1 11.5m 500/0/0
r vs v2 18.8m 497/3/0

v1 vs v2 36.6m 467/33/0
v2 vs v1 33.4m 406/94/0

v1 vs v1 46.5 134/364/2
v2 vs v2 46.5m 136/360/4

http://localhost/p5/3d4g/index.html
*/
"use strict";
let modules_to_load = ['./gameboard.js', './constants.js', './statistics.js', './ai.js'];
let loaded = modules_to_load.length;
for(let i = 0; i<modules_to_load.length; i++){
		import(modules_to_load[i]).then(module => {loaded--;console.log("got " + modules_to_load[i]);});
}

// game data structure
let board;
let current_player = 1;
let winner = 0;

let game_moves = 0, game_duration = 0, stalling = 0, mps = [100, 0];
let history = [[], [], []];

let wins = [0, 0, 0];
let last_bead = [0,0,0];

let passed = 0;
let step = 0;


function setup() {
  createCanvas(390, 160);
  //create loading screen
  frameRate(120);
  while(loaded>0){
		print("Waiting for" + loaded + " modules...");
		//animate loadingscreen
  }
  
  board = newGameboard();
  print(board);
  // test area do not take seriously!!!!
  /*let test_bead = [1, 1, 2];
  let intersect_test = [[[0, 1, 1], [0, 0, 0], [0, 2, 2], [0, 3, 3]]];
  intersect_test = getIntersectingLines(test_bead);
  print(intersect_test);
  for(let n = 0; n < intersect_test.length; n++){
  		for(let m = 0;m<4; m++){
			beads[intersect_test[n][m][0]][intersect_test[n][m][1]][intersect_test[n][m][2]] = 1;
  		}
	}*/
  	//beads[test_bead[0]][test_bead[1]][test_bead[2]] = -1;
	//print(checkBead(test_bead));
	
	//print(checkBead([2, 0, 0]));
	// end of test area /s
}

function draw() {
  background(220);
  //TODO test area do not take seriously!!!!
  passed -= deltaTime;
  game_duration += deltaTime;
  
  if(passed <= 0){
  		// reset field
		if(checkBead(last_bead) == 0 && stalling < 100){
			let rx = random([0, 1, 2, 3]);
			let ry = random([0, 1, 2, 3]);
			if(1){
				// keep the best coordinate with its score
				let best_score_coordinate = [0, 0];
				let best_score = -2000;
				
				// iterate all stakes
				for(let i = 0; i<=3; i++){
					let line = [];
					for(let j = 0; j<=3; j++){
						// calculate stake score
						let height = getHighest(i, j)[0];
						// temporary variable for summing stake score
						let intersection_score = 0;
						if(height<=3){
								let zwickmuele = [0, 0];
								// generate and iterate through intersecting axis
								let intersections = getIntersectingLines([i, j, height]);
								for(let k = 0; k < intersections.length; k++){
									// calculate score for axis and add it to the intersection score
									let beads_on_axis = beadsOnAxis(intersections[k]);
									if(1){
										if(beads_on_axis[(current_player).toString()]==3){
											intersection_score += 60000;
										}
										if(beads_on_axis[(current_player*-1).toString()]==0){
											if(beads_on_axis[(current_player).toString()]==2){
												intersection_score += 1;
											}
											if(beads_on_axis[(current_player).toString()]==1){
												intersection_score += 2;
											}
											if(beads_on_axis[(current_player).toString()]==2){
												if(zwickmuele[0] == 1){
													intersection_score += 5;
												}
												zwickmuele[0] += 1;
											}
										}
										if(beads_on_axis[(current_player).toString()]==0){
											if(beads_on_axis[(current_player*-1).toString()]==0){
												intersection_score += 4;
											}else if(beads_on_axis[(current_player*-1).toString()]==3){
												intersection_score += 5000;
											}else{
												intersection_score += beads_on_axis[(current_player*-1).toString()];
											}
											if(beads_on_axis[(current_player*-1).toString()]==2){
												if(zwickmuele[1] == 1){
													intersection_score += 4;
												}
												zwickmuele[1] += 1;
											}
										}
								// step ahead vvv
										if(height < 3){
											let zwickmuele_ahead = [0, 0];
											board[i][j][height] = current_player;
											let intersections_ahead = getIntersectingLines([i, j, height+1]);
											for(let k_ahead = 0; k_ahead < intersections_ahead.length; k_ahead++){
												let beads_on_axis_ahead = beadsOnAxis(intersections_ahead[k_ahead]);
												if(beads_on_axis_ahead[(current_player).toString()]==3){
													intersection_score += -0.1;
												}
												if(beads_on_axis_ahead[(current_player*-1).toString()]==0){
													if(beads_on_axis_ahead[(current_player).toString()]==2){
														intersection_score += 0.01;
													}
													if(beads_on_axis_ahead[(current_player).toString()]==1){
														intersection_score += 0.02;
													}
													if(beads_on_axis_ahead[(current_player).toString()]==2){
													if(zwickmuele_ahead[0] == 1){
														intersection_score += 0.08;
													}
													zwickmuele_ahead[0] += 1;
												}
												}
												if(beads_on_axis_ahead[(current_player).toString()]==0){
													if(beads_on_axis_ahead[(current_player*-1).toString()]==3){
														intersection_score += -500;
													}
													if(beads_on_axis_ahead[(current_player*-1).toString()]==2){
														if(zwickmuele_ahead[1] == 1){
															intersection_score += -0.2;
														}
														zwickmuele_ahead[1] += 1;
													}
												}
											}
											board[i][j][height] = 0;
										}
								// step ahead ^^^
									}else{
										if(beads_on_axis[(current_player*-1).toString()]==3){
											intersection_score += 500;
										}
										if(beads_on_axis[(current_player*-1).toString()]==2){
											if(zwickmuele[1] == 1){
												intersection_score += 20;
											}
											zwickmuele[1] += 1;
										}
										if(beads_on_axis[(current_player).toString()]==2){
											if(zwickmuele[0] == 1){
												intersection_score += 20;
											}
											zwickmuele[0] += 1;
										}
										if(beads_on_axis[(current_player*-1).toString()]){
											intersection_score += -beads_on_axis[(current_player*-1).toString()]*40;
										}
										
										if(beads_on_axis[current_player.toString()]==3){
											intersection_score += 1000;
										}else{
											intersection_score += beads_on_axis[current_player.toString()]*40;
										}
									}
								}
								if((intersection_score > best_score) || (intersection_score == best_score) && (floor(random((i*4+j))) <= 1)){
									best_score_coordinate = [i, j];
									best_score = intersection_score;
								}
						}else{
							intersection_score = -1000;
						}
						//line.push(intersection_score);
					}
					//print("     " + line.toString());
				}
				//print(nfs(current_player) + " > " + nfs(best_score, 4) + " at " + best_score_coordinate.toString());
				rx = best_score_coordinate[0];
				ry = best_score_coordinate[1];
			}
			let hi = getHighest(rx, ry);
			if(hi[0] <= 3){
				game_moves++;
				stalling = 0;
				last_bead = [rx, ry, hi[0]];
				dropBead([rx, ry], current_player, board);
				current_player *= -1;
			}
			else{
				stalling++;
				step--;
			}
		}else{
			passed+=200;
			let w = checkBead(last_bead);
			if(w == 1){
				wins[0]++;
			}else if(w == -1){
				wins[1]++;
			}else{
				wins[2]++;
			}
			if(w == -1){
				history[2].push([game_moves, game_duration]);
			}else{
				history[w].push([game_moves, game_duration]);
			}
			let cur_mps = game_moves/(game_duration/1000)
			if(cur_mps > mps[1]){mps[1] = cur_mps;}
			print("game " + (wins[0] + wins[1] + wins[2]) + " " + nfs(w) + " won " + nf(100/wins[0]*(wins[0]-wins[1]), 4, 1) + "% p1=" + wins[0] + " p-1=" + wins[1] + " stl=" + wins[2] + " " + nf(game_moves, 2) + " mov " + nf((game_duration/1000), 2, 3) + " sec " + nf(mps[1], 1, 1) + ">" + nf(cur_mps, 1, 1) + " mov/sec average " + nf((step/(wins[0] + wins[1] + wins[2])), 2, 1) + " moves");
			game_moves = 0;
			game_duration = 0;
			current_player = 1;
			stalling = 0;
			let output = "";
			if((wins[0] + wins[1] + wins[2]) % 500 == 0){
				for(let i = 0; i<=2; i++){
					for(let j = 0; j<history[i].length; j++){
						output+=history[i][j].toString()+"," + i + "\n";
					}
				}
				print(output);
			}
			for(let x = 0;x<=3; x+=1){
		  		for(let y = 0; y<=3; y+=1){
			  		for(let z = 0;z<=3; z+=1){
		  				beads[x][y][z] = 0;
			  		}
				}
			}
	   }
		// calculate test bead and intersecting lines
		/*
		let test_bead = [step&3, (step&12)>>2, (step&48)>>4];
  		let intersect_test = getIntersectingLines(test_bead);
		for(let n = 0; n < intersect_test.length; n++){
  			for(let m = 0;m<4; m++){
				beads[intersect_test[n][m][0]][intersect_test[n][m][1]][intersect_test[n][m][2]] = 1;
  			}
		}*/
  		//beads[test_bead[0]][test_bead[1]][test_bead[2]] = -1;
		passed += 1;
		step += 1;
	}
	//TODO end of test area /s
  drawField(40, 10, 80, 85, 12, 18);
  // Ohne zeichnen 47.6<58.2<58.8
  // Mit zeichnen  45.7<58.5<59.4
}

// Graphics Functions:

function drawField(x_offset, y_offset, height, width, depth, offset, thickness = 10, height_extra = 5, board){
	//TODO redesign stake and bead drawing so that each row is drawn on top of the others behind, not each collumn. and stakes are drawn on top of the beads of further behind rows, not behind.
	//TODO make generation of bead_diameter/depth/width/offset/extra_height dependent (with a default relative scaling factor) on one size parameter.
	// Board
	fill(board_color);
	rect(x_offset + offset * 3, y_offset + height + depth * 4 + height_extra, width * 3.4, thickness);
	quad(
	x_offset + offset * 3, y_offset + height + depth * 4 + height_extra, 
	x_offset + offset * 3, y_offset + height + depth * 4 + height_extra + thickness, 
	x_offset - offset * 2, y_offset + height + height_extra + thickness - depth,
	x_offset - offset * 2, y_offset + height + height_extra - depth
	);
	quad(
	x_offset + offset * 3, y_offset + height + depth * 4 + height_extra, 
	x_offset + offset * 3 + width * 3.4, y_offset + height + depth * 4 + height_extra, 
	x_offset - offset * 2 + width * 3.4, y_offset + height + height_extra - depth,
	x_offset - offset * 2, y_offset + height + height_extra - depth
	);

	// Stakes
	drawStakes(x_offset, y_offset, height, width, depth, offset, height_extra);
	
	// Beads
	drawBeads(x_offset, y_offset, width, depth, offset, height / 4, height_extra, board);
}

function drawStakes(x_offset, y_offset, height, width, depth, offset, height_extra = 5){
	for(let i = 0;i<4; i+=1){
  		for(let j = 0; j<4; j+=1){
	  		//draw stake
			line(x_offset + width * i + offset * j, 
				  y_offset + depth * j + height + height_extra, 
				  x_offset + width * i + offset * j, 
				  y_offset + depth * j);
		}
	}
}

function drawBeads(x_offset, y_offset, width, depth, offset, bead_diameter, height_extra = 5, board){
	for(let i = 0;i<4; i+=1){
  		for(let j = 3; j>=0; j-=1){
	  		for(let k = 3;k>=0; k-=1){
  				//draw bead at beads[i][j][k]; 
  				if(beads[i][j][k] != 0){
  					// color the beads black or white
  					if(beads[i][j][k] == 1){
  						fill(bead_color[0]);
  					}else if(beads[i][j][k] == -1){
  						fill(bead_color[1]);
  					}else{
  						print("Invalid value (" + beads[i][j][k] + ") at [" + i + ", " + j + ", " + k +"]");
					}
  					circle(x_offset + width * i + offset * (3 - j), y_offset + depth * ( 3 - j ) + bead_diameter * (3-k + 0.5) + height_extra, bead_diameter);
				}
	  		}
		}
	}
}
